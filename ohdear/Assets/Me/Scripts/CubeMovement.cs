﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovement : MonoBehaviour
{

    public float dist;
    public Rigidbody rb;

    Vector3 force;
    Vector3 leftbound;
    Vector3 rightbound;
    Vector3 backbound;
    Vector3 originalPos;
    Quaternion originalAng;
    bool right;
    bool push;
    bool getBack;
    Vector3 temp = new Vector3();
    Vector3 off = new Vector3();

    float distance = 0;
    Vector3 safe = new Vector3();

    bool floor;

    // Use this for initialization
    void Start()
    {
        off = transform.position;
        rb = GetComponent<Rigidbody>();
        leftbound = transform.position - new Vector3(dist, 0, 0);
        rightbound = transform.position + new Vector3(dist, 0, 0);
        backbound = transform.position + new Vector3(0, 0, dist);
        originalPos = transform.position;
        originalAng = rb.rotation;
        right = true;
        push = true;
        getBack = false;
        floor = false;
        force = new Vector3(0, 0.1f, 2);
    }

    // Update is called once per frame
    void Update()
    {
        die();
        if (!floor)
            transform.Rotate(transform.right, 7f);
    }

    void FixedUpdate()
    {

    }

    void OnCollisionEnter(Collision collision)
    {
        floor = true;
        //transform.rotation = new Quaternion();
        GameObject me = GameObject.Find("Cube1");
        Destroy(me,1f);
    }

    void blindedByLight()
    {
        if (right)
        {
            transform.position += new Vector3(0.1f, 0.1f, 0);
            if (transform.position.x > rightbound.x)
                right = false;
        }
        else
        {
            transform.position -= new Vector3(0.1f, 0.1f, 0);
            if (transform.position.x < leftbound.x)
                right = true;
        }
    }

    void die()
    {
        rb.useGravity = true;
    }

    void fireballToTheFace()
    {
        if (push)
        {
            temp = off;
            temp.z += Mathf.Sin(Time.fixedTime * Mathf.PI) * 0.1f;
            transform.position = temp;
            off = transform.position;
            transform.Rotate(transform.right, Mathf.Sin(Time.fixedTime * Mathf.PI * 2) * 5f);
            if (transform.position.z - originalPos.z > distance)
                distance = transform.position.z - originalPos.z;

            if (transform.position.z >= backbound.z)
            {
                safe = transform.position;
                getBack = true;
                push = false;
            }
        }
        if (getBack)
        {
            temp.z += (originalPos.z - safe.z) * 0.028f;
            transform.position = temp;
            //transform.Rotate(transform.right, Mathf.Sin(Time.fixedTime * Mathf.PI * 2) * 5f);
            if (transform.position.z <= originalPos.z)
            {
                transform.position = originalPos;
                getBack = false;
            }
        }
    }
}
