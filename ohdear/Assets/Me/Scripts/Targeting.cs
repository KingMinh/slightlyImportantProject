﻿using cakeslice;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using lockedOn;


public class Targeting : MonoBehaviour
{

    public GameObject direction;
    private Outline outl;
    public LineRenderer ray;
    public float rayLength;
    public float rayWidth;

    // Use this for initialization
    void Start()
    {
        Vector3[] initLaserPositions = new Vector3[2] { transform.position, transform.position };
        ray.SetPositions(initLaserPositions);
        ray.startWidth = rayWidth / 10;
        ray.endWidth = rayWidth / 10;
        ray.material = new Material(Shader.Find("Particles/Additive"));
        ray.startColor = Color.blue;
        ray.endColor = Color.cyan;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Vector3 dir = (direction.transform.position - transform.position);
        Ray rayray = new Ray(transform.position, dir); Debug.DrawRay(transform.position, dir);
        GameObject rh = GameObject.Find("RightController");
        Selecting s = rh.GetComponent<Selecting>();
        dir.Normalize();
        ray.SetPosition(0, transform.position);
        ray.SetPosition(1, transform.position + dir * rayLength);

        if (Physics.Raycast(rayray, out hit))
        {
            if (hit.collider.tag == "Enemy" && (PartnerBehavior.selectedAtk == TurnSystem.partnerTurn))
            {
                GameObject go = hit.collider.gameObject;
                
                s.canShoot = true;
                if (hit.collider.name != "mutant")
                {
                    Outline meh = go.GetComponent<Outline>();
                    if (meh == null)
                    {
                        Transform mesh = go.transform.Find("mesh");
                        meh = mesh.gameObject.GetComponent<Outline>();
                    }
                    meh.eraseRenderer = false;

                    if (PartnerBehavior.selectedAtk)
                        meh.color = 2;
                    else
                        meh.color = 0;
                    s.target = go;
                    removeOutlines(go);
                } else
                {
                    s.target = go.transform.Find("Mutant:Hips").gameObject;
                    if (PartnerBehavior.selectedAtk)
                        s.target = go;
                }
                if (go.GetComponent<EnemyBehavior>() != null)
                {
                    EnemyBehavior eb = go.GetComponent<EnemyBehavior>();
                    eb.createHealthBar();
                }

            }
            if (hit.collider.tag == "Move")
            {
                if ((PartnerBehavior.selectedAtk && hit.collider.name == "Attack") || !PartnerBehavior.selectedAtk)
                {
                    GameObject go = hit.collider.gameObject;
                    Outline ol = go.transform.Find("Cube").GetComponent<Outline>();

                    ol.eraseRenderer = false;
                    s.target = go;
                    removeOutlines(go);
                }
            }

        }
    }

    void removeOutlines(GameObject go)
    {
        GameObject[] array = GameObject.FindGameObjectsWithTag("Enemy");
        if (array[0].name != "mutant")
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] != go)
                {

                    array[i].GetComponent<Outline>().eraseRenderer = true;
                    if (array[i].GetComponent<EnemyBehavior>() != null)
                        array[i].GetComponent<EnemyBehavior>().deleteHealthBar();
                }
            }
        }
        if (GameObject.Find("MoveRoster(Clone)") != null)
        {
            foreach (Transform child in GameObject.Find("MoveRoster(Clone)").transform)
            {
                if (PartnerBehavior.selectedAtk && child.name == "Attack") { }
                else
                if (go == null || go.name != child.name)
                    child.Find("Cube").GetComponent<Outline>().eraseRenderer = true;
            }
        }

    }

    public static void dye(GameObject go, int color)
    {
        Outline ol = go.transform.Find("Cube").GetComponent<Outline>();
        ol.color = color;
    }

    public static void dyeEnemy(GameObject go, int color)
    {
        Outline o = go.GetComponent<Outline>();
        o.color = color;
    }
}
