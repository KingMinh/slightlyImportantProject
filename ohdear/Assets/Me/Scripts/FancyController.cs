﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

public class FancyController : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    public OVRInput.Controller controller;

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = OVRInput.GetLocalControllerPosition(controller);
        transform.localRotation = OVRInput.GetLocalControllerRotation(controller);

        if (OVRInput.GetDown(OVRInput.Button.Two) && !Selecting.done)
        {
            Selecting.option++;
            Selecting.option %= Selecting.max;
        }

        //Debug.Log(OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger,controller));


    }
}
