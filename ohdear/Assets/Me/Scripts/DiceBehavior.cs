﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiceBehavior : MonoBehaviour
{

    public float amp;
    public bool ani;
    public bool blowUp;
    public bool blindedByLight;
    public bool explosion;
    public bool atk;
    public GameObject EXM;

    GameObject Player;
    Rigidbody rb;
    EnemyBehavior eb;
    Vector3 temp = new Vector3();
    Vector3 off = new Vector3();
    Vector3 dir;
    Vector3 oriPos;
    Vector3 startPos;
    bool right = true;
    bool up = true;
    bool attacking;
    bool flyBack;

    float rightBound;
    float leftBound;
    float ceil;
    float floor;
    float timer;
    float currentLerpTime;
    int seconds;

    AudioSource source;

    // Use this for initialization
    void Start()
    {
        off = transform.position;
        Player = GameObject.Find("Playa");
        eb = gameObject.GetComponent<EnemyBehavior>();
        rb = gameObject.GetComponent<Rigidbody>();
        ani = false;
        blowUp = false;
        
        rightBound = transform.position.x + 0.1f;
        leftBound = transform.position.x - 0.1f;
        ceil = transform.position.y + 0.1f;
        floor = transform.position.y - 0.1f;
        timer = 0;
        oriPos = transform.position;
        attacking = true;
        flyBack = false;
        source = GetComponent<AudioSource>();
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float hp = eb.hitpoints / eb.maxHP;
        if (hp < 0.3f && !ani && Selecting.done && !blindedByLight)
        {
            lowOnHealth();
        }
        else if (blindedByLight && !Selecting.done && !ani)
        {
            hitByRightning();
        }
        else if (explosion && !ani)
        {
            timer += Time.deltaTime;
            seconds = (int)timer % 60;
            getBack();
        }
        else if (atk && !ani)
        {
            if (attacking)
            {
                Vector3 dir = Player.transform.position - oriPos;
                dir.Normalize();
                Vector3 direction = new Vector3(dir.x, dir.y, dir.z);
                direction.Normalize();
                transform.position += dir * 0.65f;
            }
            if (flyBack)
            {
                currentLerpTime += Time.deltaTime;
                if (currentLerpTime > 1f)
                {
                    currentLerpTime = 1f;
                }

                float perc = currentLerpTime / 1f;
                if (perc >= 1f)
                {
                    currentLerpTime = 0f;
                    rb.velocity = Vector3.zero;
                    rb.angularVelocity = Vector3.zero;
                    flyBack = false;
                    attacking = true;
                    atk = false;
                    EnemyBehavior.waitUp = false;
                }
                else
                {
                    transform.position = Vector3.Lerp(startPos, oriPos, perc);
                }
            }
        }
        else
        {
            // idle animation
            temp = off;
            temp.y += Mathf.Sin(Time.fixedTime * Mathf.PI) * 0.01f;
            transform.position = temp;
            blindedByLight = false;
        }
        // self-destruct
        if (ani)
        {
            dir = Player.transform.Find("Main Camera").transform.position - transform.position;
            float x = Mathf.Abs(dir.x);
            float z = Mathf.Abs(dir.z);
            dir.Normalize();
            transform.position += dir * 0.1f;
            x = Mathf.Abs(Player.transform.position.x - transform.position.x);
            z = Mathf.Abs(Player.transform.position.z - transform.position.z);
            if (z <= 2)
            {
                blowUp = false;
                ani = false;
                EnemyBehavior.waitUp = false;
                SelfDestruct();
            }
        }
        off = transform.position;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Hitbox" && attacking)
        {
            attacking = false;
            rb.useGravity = true;
            PlayerData pd = Player.GetComponent<PlayerData>();
            pd.takeDmg(4);
            source.PlayOneShot(source.clip);
        }
        if (collision.gameObject.name == "Floor")
        {
            rb.useGravity = false;
            flyBack = true;
            startPos = transform.position;
        }
    }

    public void SelfDestruct()
    {
        Instantiate(EXM, transform.position, new Quaternion());
        PlayerData pd = Player.GetComponent<PlayerData>();
        pd.takeDmg(100000);
        GameObject nuke = GameObject.Find(this.name);
        Destroy(nuke, 0.5f);
    }

    void lowOnHealth()
    {
        if (right)
        {
            transform.position += new Vector3(0.1f, 0.1f, 0);
            if (transform.position.x > rightBound)
                right = false;
        }
        else
        {
            transform.position -= new Vector3(0.1f, 0.1f, 0);
            if (transform.position.x < leftBound)
                right = true;
        }
    }

    void hitByRightning()
    {
        if (up)
        {
            transform.position += new Vector3(0, 0.1f, 0);
            if (transform.position.y > ceil)
                up = false;
        }
        else
        {
            transform.position -= new Vector3(0, 0.1f, 0);
            if (transform.position.y < floor)
                up = true;
        }
    }

    IEnumerator boom()
    {
        Vector3 dir = Player.transform.position - transform.position;
        float x = Mathf.Abs(dir.x);
        float z = Mathf.Abs(dir.z);
        dir.Normalize();
        while (z > 0 && x > 0)
        {
            Transform parent = transform.parent;
            parent.position += dir * 0.5f;
            Debug.Log(parent.position);
            yield return new WaitForEndOfFrame();
            x = Mathf.Abs(Player.transform.position.x - transform.position.x);
            z = Mathf.Abs(Player.transform.position.z - transform.position.z);
        }
        ani = false;
    }

    void getBack()
    {
        if (seconds >= 1)
        {
            if (transform.position.y <= oriPos.y)
            {
                explosion = false;
                timer = 0;
                rb.useGravity = false;
                rb.angularVelocity = Vector3.zero;
                rb.velocity = Vector3.zero;
                transform.position = oriPos;
                Selecting.wait = false;
                Selecting.done = true;
            }
        }
        else
        {
            rb.useGravity = true;
        }
    }

}
