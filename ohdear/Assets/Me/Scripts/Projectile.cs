﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    public static bool fly;
    public Rigidbody rb;
    public GameObject boom;
    public GameObject bam;
    public Vector3 target;

    public float damaji;
    float currentLerpTime;

    Vector3 dir;
    Vector3 oriPos;
    // Use this for initialization
    void Start()
    {
        fly = false;
        rb = GetComponent<Rigidbody>();
        dir = target - transform.position;
        dir.Normalize();
        oriPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        currentLerpTime += Time.deltaTime;
        if (currentLerpTime > 1f)
        {
            currentLerpTime = 1f;
        }

        float perc = currentLerpTime / 1f;

        transform.position = Vector3.Lerp(oriPos, target, perc);
        //if (OVRInput.GetDown(OVRInput.Button.Two))
        //    Destroy(gameObject);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Instantiate(bam, transform.position, new Quaternion());
            GameObject enemeh = collision.gameObject;
            if (enemeh.GetComponent<EnemyBehavior>() != null)
            {
                EnemyBehavior eb = enemeh.GetComponent<EnemyBehavior>();
                float damage = eb.takeDMG(damaji, "fire");

                // show dmg prompt
                GameObject player = GameObject.Find("Playa");
                PlayerData pd = player.GetComponent<PlayerData>();

                if (eb.hitpoints <= 0 && collision.ToString() != "mutant")
                {
                    Transform overlord = enemeh.transform.parent;
                    foreach (Transform child in overlord)
                    {
                        Destroy(child.gameObject);
                    }
                    Selecting.done = true;
                    Selecting.wait = false;
                }
                
                
                // set prompt position
                Vector3 pp = enemeh.transform.position;

                if (enemeh.name == "mutant")
                {
                    pp = enemeh.transform.Find("Mutant:Hips").position;
                    pp += new Vector3(1.4f, 0);
                } else
                {
                    Vector3 playerPos = player.transform.position;
                    Vector3 enemyPos = enemeh.transform.position;
                    Vector3 dir = enemyPos - playerPos;
                    dir.Normalize();
                    pp -= dir;
                    pp += new Vector3(0, 2, 0); 
                }
                pd.showDMG(damage, pp);
                pd.setFlames(false);
                Selecting.max = 3;

                // enemy reaction
                if (collision.gameObject.name != "mutant")
                {
                    enemeh.GetComponent<Rigidbody>().AddExplosionForce(500, transform.position - new Vector3(0, 10, 0), 30, 5);
                    DiceBehavior db = enemeh.GetComponent<DiceBehavior>();
                    db.explosion = true;
                    Destroy(gameObject);
                }
                else
                {
                    TheBoss B = enemeh.GetComponent<TheBoss>();
                    B.getHit(true);
                    StartCoroutine(bossHit());
                }

            }
            else
            {
                Pills p = enemeh.GetComponent<Pills>();
                p.transition();
                Destroy(gameObject);    
            }
            
        }
    }

    IEnumerator bossHit()
    {
        yield return new WaitForSeconds(1);
        Selecting.wait = false;
        Selecting.done = true;
        Destroy(gameObject);
    }

    /*
    private void OnTriggerEnter(Collider other)
    {
        Instantiate(bam, transform.position, new Quaternion());
        GameObject enemeh = other.gameObject;
        EnemyBehavior eb = enemeh.GetComponent<EnemyBehavior>();
        eb.takeDMG(damaji);

        // show dmg prompt
        GameObject player = GameObject.Find("Playa");
        PlayerData pd = player.GetComponent<PlayerData>();
        pd.showDMG(damaji, enemeh.transform.parent.name);

        Transform overlord = enemeh.transform.parent;
        if (eb.hitpoints <= 0)
        {
            foreach (Transform child in overlord)
            {
                Destroy(child.gameObject);
            }
        }
        Destroy(gameObject);

        enemeh.GetComponent<Rigidbody>().AddExplosionForce(500, transform.position - new Vector3(0, 10, 0), 30, 5);
        DiceBehavior db = enemeh.GetComponent<DiceBehavior>();
        db.explosion = true;

    }
    */

}
