﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;

namespace lockedOn
{

    public class Targeted : MonoBehaviour
    {
        public bool targeted;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (!targeted)
            {
                Outline line = GetComponent<Outline>();
                line.eraseRenderer = true;
            }
        }

    }
}