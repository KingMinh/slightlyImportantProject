﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
// 26, 28, 29, 30
public class PartnerBehavior : MonoBehaviour
{

    Animator anim;
    AnimatorStateInfo currentBaseState;
    AudioSource source;
    bool atk;
    bool runup;
    bool runback;
    public static bool wait;

    Vector3 dir;
    Vector3 oriPos;
    Quaternion oriRot;
    Quaternion targetRot;
    Vector3 startPos;
    Quaternion startRot;
    float currentLerpTime;
    int frameCounter;


    //stats
    public float maxHP;
    public float hp;

    public GameObject stats;
    public GameObject target;
    public Canvas roster;
    public ParticleSystem highlight;
    public GameObject healEffect;
    public GameObject buffEffect;
    public AudioClip healS;
    public AudioClip buffS;

    public static bool selectedAtk;
    public static int buffStacks;

    GameObject player;
    // Use this for initialization
    void Start()
    {
        hp = maxHP;
        frameCounter = 0;
        anim = GetComponent<Animator>();
        source = GetComponent<AudioSource>();
        
        atk = false;
        runback = false;

        oriPos = transform.position;
        oriRot = transform.rotation;

        player = GameObject.FindGameObjectWithTag("Player");
        showHealthbar();
    }

    void FixedUpdate()
    {
        currentBaseState = anim.GetCurrentAnimatorStateInfo(0);
        /*if (Input.GetKeyDown("a"))
        {
            Debug.Log("RunToEnemy");
            anim.SetBool("runUp", true);
            atk = true;
            runup = true;
        }
        if (Input.GetKeyDown("c"))
        {
            anim.SetBool("heal", true);
        }
        if (Input.GetKeyDown("b"))
        {
            anim.SetTrigger("buff");
        }
        */
    }

    void Update()
    {
        if (currentBaseState.IsName("Heal"))
            heal();

        if (wait && currentBaseState.IsName("Idle"))
        {
            TurnSystem.partnerTurn = false;
            wait = false;
            frameCounter = 0;
            GameObject[] go1 = GameObject.FindGameObjectsWithTag("healEffect");
            if (go1.Length != 0)
            {
                for (int i = 0; i < 2; i++)
                {
                    Destroy(go1[i]);
                }
            }
        }
        if (atk)
        {
            atkLoop();
        }
    }

    void atkLoop()
    {
        if (runup)
        {
            dir = target.transform.position - transform.position;
            dir = new Vector3(dir.x, 0, dir.z);
            dir.Normalize();
            transform.position += dir * 0.1f;
            transform.rotation = targetRot;
            Vector3 fill = transform.position - target.transform.position;
            Vector3 fill2 = new Vector3(fill.x, 0, fill.z);
            float dist = fill2.magnitude;
            if (dist < 1.2f)
            {
                anim.SetBool("atk", true);
                anim.SetBool("runBack", true);
                runup = false;
                startPos = transform.position;
                startRot = transform.rotation;
            }

        }
        else if (currentBaseState.IsName("Punch"))
        {
            anim.SetBool("runBack", true);
            runback = true;
        }
        else if (runback)
        {
            currentLerpTime += Time.deltaTime * 1.2f;
            if (currentLerpTime > 1f)
            {
                currentLerpTime = 1f;
            }

            float perc = currentLerpTime / 1f;
            if (perc >= 1)
            {
                wait = true;
                currentLerpTime = 0;
                runback = false;
                atk = false;
                selectedAtk = false;
                anim.SetBool("runUp", false);
                anim.SetBool("atk", false);
                anim.SetBool("runBack", false);
                endHighlight();
                // remove dmg prompt
                GameObject prompt = GameObject.Find("DamagePrompt(Clone)");
                if (prompt != null)
                    Destroy(prompt);
            }
            else
            {
                transform.position = Vector3.Lerp(startPos, oriPos, perc);
                transform.rotation = Quaternion.Lerp(startRot, oriRot, perc);

            }
        }
    }

    public void initiateAtk()
    {
        anim.SetBool("runUp", true);
        atk = true;
        runup = true;
        Vector3 tr = new Vector3(target.transform.position.x, 0, target.transform.position.z);
        Vector3 pos = new Vector3(transform.position.x, 0, transform.position.z);
        targetRot = Quaternion.LookRotation(tr - pos, Vector3.up);
    }

    public void initiateHeal()
    {
        anim.SetBool("heal", true);
    }

    public void initiateBuff()
    {
        anim.SetTrigger("buff");

    }

    void punch()
    {
        source.Play();
        PlayerData pd = player.GetComponent<PlayerData>();
        Vector3 pos = GameObject.Find("Mutant:RightLeg").transform.position + new Vector3(-1.5f, 0);
        EnemyBehavior eb = GameObject.Find("mutant").GetComponent<EnemyBehavior>();
        float dmg = eb.takeDMG(20, "atk");
        pd.showDMG(dmg, pos);
        TheBoss B = GameObject.Find("mutant").GetComponent<TheBoss>();
        B.getHit(false);
    }

    void heal()
    {
        frameCounter++;
        anim.SetBool("heal", false);
        // TODO: Heal Calculations
        if (frameCounter == 30)
        {
            endHighlight();
            GameObject go1 = Instantiate(healEffect);
            go1.transform.position = transform.position;
            GameObject go2 = Instantiate(healEffect);
            Vector3 floor = GameObject.Find("Floor").transform.position;
            float dist = floor.y - player.transform.Find("Main Camera").position.y;
            go2.transform.position = player.transform.Find("Main Camera").position + new Vector3(0, dist, 0);
            hp = maxHP; // balance patch inc
            GameObject healthbar = GameObject.Find("UCUI(Clone)").transform.Find("HealthBar").gameObject;
            HealthBar hb = healthbar.GetComponent<HealthBar>();
            float perc2 = hp / maxHP;
            hb.showHealth(perc2);
            Text text = GameObject.Find("UCUI(Clone)").transform.Find("HealthNum").GetComponent<Text>();
            text.text = hp + "/" + maxHP;
            source.PlayOneShot(healS, 0.5f);
            PlayerData pd = player.GetComponent<PlayerData>();
            pd.fullHeal();
        }
        wait = true;
    }

    void buffFX()
    {
        wait = true;
        endHighlight();
        GameObject go1 = Instantiate(buffEffect);
        go1.transform.position = transform.position;
        GameObject go2 = Instantiate(buffEffect);
        Vector3 floor = GameObject.Find("Floor").transform.position;
        float dist = floor.y - player.transform.Find("Main Camera").position.y;
        go2.transform.position = player.transform.Find("Main Camera").position + new Vector3(0, dist, 0);

    }

    void applyBuff()
    {
        buffStacks++;
        GameObject ui = GameObject.Find("UCUI(Clone)").gameObject;
        if (buffStacks == 1)
        {
            player.transform.Find("Main Camera").Find("PlayerUI").Find("BuffImg").gameObject.SetActive(true);
            player.transform.Find("Main Camera").Find("PlayerUI").Find("BuffMultiplicator").gameObject.SetActive(true);
            ui.transform.Find("BuffImg").gameObject.SetActive(true);
            ui.transform.Find("BuffMult").gameObject.SetActive(true);
        }
        if (buffStacks > 3)
            buffStacks = 3;
        Text t1 = player.transform.Find("Main Camera").Find("PlayerUI").Find("BuffMultiplicator").GetComponent<Text>();
        Text t2 = ui.transform.Find("BuffMult").GetComponent<Text>();
        t1.text = "x" + buffStacks;
        t2.text = "x" + buffStacks;
    }

    void playBuffSound()
    {
        source.PlayOneShot(buffS); 
    }

    public void showReceivedDMG(float dmg)
    {
        GameObject prompt = GameObject.Find("UCUI(Clone)").transform.Find("DamageNum").gameObject;
        prompt.SetActive(true);
        Text t = prompt.GetComponent<Text>();
        t.text = "" + dmg;
    }

    public void hideReceivedDMG()
    {
        GameObject prompt = GameObject.Find("UCUI(Clone)").transform.Find("DamageNum").gameObject;
        prompt.SetActive(false);
    }

    public void die()
    {
        anim.SetTrigger("death");
    }

    public void showMoves()
    {
        GameObject go = GameObject.Find("MoveRoster(Clone)");
        if (go == null)
        {
            Instantiate(roster);
            GameObject ui = GameObject.Find("MoveRoster(Clone)");
            Vector3 dir = transform.position - player.transform.position;
            dir.Normalize();
            //ui.transform.position -= dir;
            ui.transform.position = transform.position;
            ui.transform.position += new Vector3(0, 2, 0);
            ui.transform.rotation = Quaternion.LookRotation(ui.transform.position - player.transform.position, Vector3.up);
        }
    }

    public void hideMoves()
    {
        GameObject go = GameObject.Find("MoveRoster(Clone)");
        Destroy(go);
    }

    public void hideMagic()
    {
        GameObject go = GameObject.Find("MoveRoster(Clone)");
        GameObject buff = go.transform.Find("Buff").gameObject;
        GameObject heal = go.transform.Find("Heal").gameObject;
        buff.SetActive(false);
        heal.SetActive(false);
    }

    public void showMagic()
    {
        GameObject go = GameObject.Find("MoveRoster(Clone)");
        GameObject buff = go.transform.Find("Buff").gameObject;
        GameObject heal = go.transform.Find("Heal").gameObject;
        buff.SetActive(true);
        heal.SetActive(true);
    }

    public void startHighlight()
    {
        GameObject go = Instantiate(highlight).gameObject;
        GameObject floor = GameObject.Find("Floor");
        go.transform.position = new Vector3(transform.position.x, floor.transform.position.y, transform.position.z);
    }

    public void endHighlight()
    {
        GameObject go = GameObject.Find("CharacterHighlight(Clone)");
        Destroy(go);
    }

    public void showHealthbar()
    {
        GameObject playa = GameObject.FindGameObjectWithTag("Player");
        Vector3 dir = playa.transform.position - transform.position;
        dir.Normalize();
        Vector3 pos = transform.position + new Vector3(0, 2, 0);

        GameObject goni = Instantiate(stats, pos, new Quaternion());
        goni.transform.position = pos;
        goni.transform.rotation = Quaternion.LookRotation(pos - playa.transform.position, Vector3.up);
        Transform t = goni.transform.Find("HealthBar");
        HealthBar h = t.gameObject.GetComponent<HealthBar>();
        float p = hp / maxHP;
        h.showHealth(p);

        Text text = goni.transform.Find("HealthNum").GetComponent<Text>();
        text.text = hp + "/" + maxHP;
    }

    public float takeDmg(float dmg)
    {
        hp -= dmg;
        showReceivedDMG(dmg);
        if (hp <= 0)
        {
            hp = 0;
            anim.SetTrigger("death");
            GameObject ui = GameObject.Find("UCUI(Clone)").gameObject;
            ui.transform.Find("BuffImg").gameObject.SetActive(false);
            ui.transform.Find("BuffMult").gameObject.SetActive(false);
        }
        else
        {
            anim.SetTrigger("hit");
        }

        hp = (int)hp;
        GameObject healthbar = GameObject.Find("UCUI(Clone)").transform.Find("HealthBar").gameObject;
        HealthBar hb = healthbar.GetComponent<HealthBar>();
        int perc = (int)(dmg / maxHP);
        float perc2 = hp / maxHP;
        hb.takeDmg(perc);
        hb.showHealth(perc2);

        Text text = GameObject.Find("UCUI(Clone)").transform.Find("HealthNum").GetComponent<Text>();
        text.text = hp + "/" + maxHP;

        return dmg;
    }


}
