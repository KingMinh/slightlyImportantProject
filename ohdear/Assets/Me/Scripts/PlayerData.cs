﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerData : MonoBehaviour
{

    public float hitpoints;
    public float maxHP;
    public bool concentrated;
    public Text healthText;
    public Canvas MovePrompt;
    public Canvas DamagePrompt;


    // Use this for initialization
    void Start()
    {
        Transform ui = transform.Find("Main Camera").Find("PlayerUI").Find("HealthNum");
        healthText = ui.gameObject.GetComponent<Text>();
        healthText.text = maxHP.ToString() + "/" + maxHP.ToString();
        hitpoints = maxHP;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void takeDmg(float dmg)
    {
        showReceivedDMG(dmg);
        hitpoints -= dmg;
        hitpoints = (int)Mathf.Max(hitpoints, 0);

        foreach (Transform child in transform)
        {
            if (child.tag == "MainCamera")
            {
                GameObject ui = child.gameObject;
                Transform t1 = ui.transform.Find("PlayerUI");
                Transform t2 = t1.Find("HealthBar");
                HealthBar h = t2.gameObject.GetComponent<HealthBar>();
                float percentage = dmg / maxHP;
                h.takeDmg(percentage);
                healthText.text = hitpoints.ToString() + "/" + maxHP.ToString();
            }
        }
    }

    public void fullHeal()
    {
        hitpoints = maxHP;
        healthText.text = hitpoints.ToString() + "/" + maxHP.ToString();
        GameObject ui = transform.Find("Main Camera").gameObject;
        Transform t1 = ui.transform.Find("PlayerUI");
        Transform t2 = t1.Find("HealthBar");
        HealthBar h = t2.gameObject.GetComponent<HealthBar>();
        h.showHealth(maxHP / maxHP); // definetely going to balance that out
    }

    public void setFlames(bool flame)
    {
        concentrated = flame;
        GameObject flames = transform.Find("Concentrate").gameObject;
        flames.SetActive(flame);
    }

    public void showPrompt(string message, string unit)
    {
        GameObject go = GameObject.Find(unit);
        Canvas ui = Instantiate(MovePrompt);

        Vector3 dir = go.transform.position - transform.position;
        dir.Normalize();
        ui.transform.position = go.transform.position;
        ui.transform.position -= dir;
        if (unit == "mutant")
        {
            ui.transform.position += new Vector3(0, 3.5f) - 2 * dir;
            ui.transform.localScale *= 2;
        }
        ui.transform.rotation = Quaternion.LookRotation(ui.transform.position - transform.position, Vector3.up);
        GameObject prompt = ui.transform.Find("Text").gameObject;
        Text t = prompt.GetComponent<Text>();
        t.text = message;
    }

    public void deletePrompt()
    {
        GameObject go = GameObject.Find("MovePrompt(Clone)");
        Destroy(go);
    }

    public void showDMG(float dmg, Vector3 pos)
    {
        Canvas canvas = Instantiate(DamagePrompt);

        /*canvas.transform.position = go.transform.position;
        Vector3 dir = go.transform.position - transform.position;
        dir.Normalize();
        ui.transform.position -= dir;
        ui.transform.position += new Vector3(0, 2, 0);*/

        canvas.transform.position = pos;
        Vector3 dir = transform.position - pos;
        dir.Normalize();
        canvas.transform.position += 2 * dir;
        canvas.transform.rotation = Quaternion.LookRotation(canvas.transform.position - transform.position, Vector3.up);
        GameObject prompt = canvas.transform.Find("Text").gameObject;
        Text t = prompt.GetComponent<Text>();
        t.text = dmg.ToString();
    }

    public void deleteDMG()
    {
        GameObject go = GameObject.Find("DamagePrompt(Clone)");
        Destroy(go);
    }

    public void showReceivedDMG(float dmg)
    {
        TurnSystem ts = GameObject.Find("Overseer").GetComponent<TurnSystem>();
        if (!ts.regAtk)
        {
            GameObject prompt = transform.Find("Main Camera").Find("PlayerUI").Find("DamageNum").gameObject;
            prompt.SetActive(true);
            Text t = prompt.GetComponent<Text>();
            t.text = "" + dmg;
        }
    }

    public void hideReceivedDMG()
    {
        GameObject prompt = transform.Find("Main Camera").Find("PlayerUI").Find("DamageNum").gameObject;
        prompt.SetActive(false);
    }

}
