﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TheBoss : MonoBehaviour
{

    public AudioClip slamSound;

    Animator anim;
    AnimatorStateInfo currentBaseState;
    GameObject target;
    GameObject fireB;
    GameObject fireG;
    GameObject elec;
    Vector3 oriPos;
    Vector3 currentPos;
    Quaternion oriRot;
    Quaternion currentRot;
    PlayerData pd;
    PartnerBehavior pb;
    EnemyBehavior eb;
    AudioSource source;

    int moveCount; // 0 = attack, 1 = elemental shift
    int frameCounter;
    bool atk;
    bool wait;
    bool flame;
    bool startCount;
    bool jumpBack;
    float currentLerpTime;

    // Use this for initialization 0.108 1.276 0.785
    void Start()
    {
        frameCounter = 0;
        anim = GetComponent<Animator>();
        oriPos = transform.position;
        oriRot = transform.rotation;
        pd = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerData>();
        pb = GameObject.FindGameObjectWithTag("Partner").GetComponent<PartnerBehavior>();
        eb = GetComponent<EnemyBehavior>();
        source = GetComponent<AudioSource>();
        fireB = GameObject.Find("Mutant:Hips").transform.Find("L_Blue_fire").gameObject;
        fireG = GameObject.Find("Mutant:Hips").transform.Find("L_Ghost_fire").gameObject;
        elec = GameObject.Find("Mutant:Hips").transform.Find("L_Eletric_ball").gameObject;
    }

    // Update is called once per frame
    void Update()
    {

        if (currentBaseState.IsName("Shift"))
        {
            wait = true;
        }
        if (wait && currentBaseState.IsName("Idle"))
        {
            frameCounter = 0;
            wait = false;
            EnemyBehavior.waitUp = false;
            startCount = false;
            jumpBack = false;
            pd.hideReceivedDMG();
            pb.hideReceivedDMG();
        }
        if (atk)
        {
            atkLoop();
        }
    }

    void FixedUpdate()
    {
        currentBaseState = anim.GetCurrentAnimatorStateInfo(0);
    }

    public void takeTurn()
    {
        moveCount++;
        moveCount %= 2;
        float thresh = eb.maxHP / 3;

        if (eb.hitpoints <= thresh)
        {
            // Dunk
            pd.showPrompt("Dunk", name);
            target = GameObject.FindGameObjectWithTag("Player");
            anim.SetTrigger("slam");
        }
        else
        {
            // Swipe
            if (moveCount == 0)
            {
                pd.showPrompt("Attack", name);
                int ran = Random.Range(0, 2); // Random.Range(0, 2)
                if (GameObject.FindGameObjectWithTag("Partner").GetComponent<PartnerBehavior>().hp <= 0)
                {
                    ran = 0;
                }
                if (ran == 0)
                {
                    target = GameObject.FindGameObjectWithTag("Player");
                    anim.SetTrigger("attack");
                    atk = true;
                }
                else if (ran == 1)
                {
                    target = GameObject.FindGameObjectWithTag("Partner");
                    Vector3 tr = new Vector3(target.transform.position.x, 0, target.transform.position.z);
                    Vector3 pos = new Vector3(transform.position.x, 0, transform.position.z);
                    transform.rotation = Quaternion.LookRotation(tr - pos, Vector3.up);
                    anim.SetTrigger("attack");
                    atk = true;
                }
            }
            // Shift
            else if (moveCount == 1)
            {
                pd.showPrompt("Elemental Shift", name);
                anim.SetTrigger("flex");
            }
        }
    }

    public void getHit(bool reaction)
    {
        if (GetComponent<EnemyBehavior>().hitpoints <= 0)
        {
            fireB.SetActive(false);
            fireG.SetActive(false);
            elec.SetActive(false);
            anim.SetTrigger("death");
        }
        else if (reaction)
        {
            anim.SetTrigger("hit");
        }
    }


    void shift()
    {

        if (!flame)
        {
            fireB.SetActive(true);
            fireG.SetActive(true);
            elec.SetActive(false);
            flame = true;
            eb.element = "fire";
        }
        else
        {
            fireB.SetActive(false);
            fireG.SetActive(false);
            elec.SetActive(true);
            flame = false;
            eb.element = "lightning";
        }
    }

    void atkLoop()
    {
        if (currentBaseState.IsName("d2"))
        {
            anim.SetTrigger("setBack");
            jumpBack = true;
        }
        else if (jumpBack)
        {
            setBack();
        }
    }

    void setT()
    {
        currentRot = transform.rotation;
        currentPos = transform.position;
    }

    void setBack()
    {
        currentLerpTime += Time.deltaTime * 2.5f;
        if (currentLerpTime > 1f)
        {
            currentLerpTime = 1f;
        }

        float perc = currentLerpTime / 1f;
        if (perc >= 1)
        {
            currentLerpTime = 0;
            atk = false;
            jumpBack = false;
            wait = true;
        }
        else
        {
            transform.rotation = Quaternion.Lerp(currentRot, oriRot, perc);
            transform.position = Vector3.Lerp(currentPos, oriPos, perc);
        }
    }

    void d2()
    {
        if (target.tag == "Partner")
        {
            pb.takeDmg(calculateDMG(60));
            setT();
        }
        else if (target.tag == "Player")
        {
            pd.takeDmg(calculateDMG(80));
            setT();
        }
        // http://en.soundeffect-lab.info/sound/battle/
    }

    void playSlashSound()
    {
        source.Play();
    }

    void dunk()
    {
        pd.takeDmg(calculateDMG(180));
        wait = true;
    }

    void playDunkSound()
    {
        source.PlayOneShot(slamSound);
    }

    float calculateDMG(float baseDMG)
    {
        return (int)(baseDMG - (PartnerBehavior.buffStacks * 0.25f) * baseDMG);
    }

    void dealDMGToPlayer(float dmg)
    {
        PlayerData pd = target.GetComponent<PlayerData>();

    }
}